package parser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
public class FinalContentParsing {
    public static void getFinalParsingString(String url) throws IOException {
        Document doc2 = Jsoup.connect(url).get();
        Elements itemElements2 = doc2.getElementsByClass("mw-parser-output");
        Elements paragraphs = itemElements2.select("p, h1, h2, h3, ul, li");
        for (Element itemElement2 : paragraphs) {
            if (itemElement2.is("h2")) {
                System.out.println("______________________________________________________________________________");
            }
            System.out.println(itemElement2.text());
        }
    }
}

