package parser;
import model.Link;
import org.json.JSONObject;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
public class LinkParser {
    public static List<Link> getLinks(String response) throws UnsupportedEncodingException {
        JSONObject jO = new JSONObject(response);
        List<Link> linkArr = new ArrayList<>();
        org.json.JSONArray jArray = jO.getJSONObject("query").getJSONArray("search");
        for (int i = 0; i < jArray.length(); i++) {
            JSONObject jObject = jArray.getJSONObject(i);
            String item = jObject.getString("title");
            if (!item.isEmpty()) {
                int id = i + 1;
                String url = "https://ru.wikipedia.org/wiki/" + URLEncoder.encode(item, "utf-8").replace("+", "_");
                linkArr.add(new Link(item, id, url));
            }
        }
        return linkArr;
    }
}
