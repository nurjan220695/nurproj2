package fetcher.impl;
import fetcher.HttpContentFetcher;
import model.HtmlResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
public class DefaultFetcherImpl implements HttpContentFetcher {
    public HtmlResponse getContent(String url) throws IOException{
        URL address = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) address.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        String str = response.toString();
        return new HtmlResponse(str, connection.getResponseCode());
    }
}









