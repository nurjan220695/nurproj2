package fetcher.impl;
import fetcher.HttpContentFetcher;
import model.HtmlResponse;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
public class UnirestFetcherImpl implements HttpContentFetcher {
    public HtmlResponse getContent(String url) throws UnirestException {
        HttpResponse response = Unirest.get(url).asString();
        return new HtmlResponse(response.getBody().toString(),response.getStatus());
    }
}