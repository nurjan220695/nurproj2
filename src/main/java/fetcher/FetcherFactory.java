package fetcher;

import fetcher.impl.DefaultFetcherImpl;
import fetcher.impl.UnirestFetcherImpl;

public class FetcherFactory {
    public static HttpContentFetcher getFetcher(FetcherType type) {
        if (type == FetcherType.UNIREST) {
            return new UnirestFetcherImpl();
        } else {
            return new DefaultFetcherImpl();
        }
    }
}
