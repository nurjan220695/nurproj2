package fetcher;
import model.HtmlResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
public interface HttpContentFetcher {
    HtmlResponse getContent(String url) throws IOException, ParserConfigurationException, SAXException, UnirestException;
}