package model;
public class HtmlResponse {
    private String txt;
    private int status;
    public HtmlResponse(String txt, int status) {
        this.status=status;
        this.txt=txt;
    }
    public int getStatus() {
        return status;
    }
    public String getTxt() {
        return txt;
    }
}
