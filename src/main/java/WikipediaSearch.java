import fetcher.FetcherFactory;
import fetcher.FetcherType;
import fetcher.HttpContentFetcher;
import model.HtmlResponse;
import parser.FinalContentParsing;
import model.Link;
import parser.LinkParser;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Scanner;
public class WikipediaSearch {
    public static void main(String[] args) throws IOException, UnirestException, ParserConfigurationException, SAXException {
        System.out.println("- Введите текст для поиска по википедии: ");
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();
        String url = "https://ru.wikipedia.org/w/api.php?action=query&format=json&srlimit=10&list=search&srsearch=" + URLEncoder.encode(text, StandardCharsets.UTF_8);
        System.out.println("Выбрать метод реализации: ");
        System.out.println("- UNIREST");
        System.out.println("- DEFAULT");
        Scanner fetchScan = new Scanner(System.in);
        String str = fetchScan.nextLine();
        FetcherType fetcherType = FetcherType.valueOf(str);
        HttpContentFetcher fetcher = FetcherFactory.getFetcher(fetcherType);
        HtmlResponse httpResponse = fetcher.getContent(url);
        LinkParser linkParser = new LinkParser();
        List<Link> links = linkParser.getLinks(httpResponse.getTxt());
        for (int l = 0; l < links.size(); l++) {
            System.out.println(links.get(l).getId() + ")" + links.get(l).getText() + " (" + links.get(l).gettUrl() + ")");}
        System.out.println("- Выберите статью для отображения: ");
        Scanner s2 = new Scanner(System.in);
        int b = s2.nextInt();
        System.out.println("- Контент: ");
        for (int i = 0; i < links.size(); i++) {
            if (b == links.get(i).getId()) {
                String url2 = links.get(i).gettUrl();
                FinalContentParsing.getFinalParsingString(url2);
            }
        }
    }
}
